# Kustelu

Simppeli keskustelufoorumi


## Toiminnat

Tällä hetkellä:

* käyttäjien rekisteröinti
* Foorumien teko
* Aiheiden aloitus
* Aiheisiin vastaaminen

Tulossa (kun kerkeää):

* Roolit
* Profiili
* "Tahmaset" aiheet
* Tiedot foorumeista/aiheista/vastauksista

