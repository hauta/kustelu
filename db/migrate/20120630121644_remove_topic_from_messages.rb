class RemoveTopicFromMessages < ActiveRecord::Migration
  def up
    remove_column :messages, :topic
  end

  def down
    add_column :messages, :topic, :boolean
  end
end
