# encoding: utf-8
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

[Forum, Topic, Message, User].each { |m| m.destroy_all }

filmi = Forum.create!(
  :name => "Filmi",
  :desc => "Kamerat, filmit, pimiö"
)

digi = Forum.create!(
  :name => "Digi",
  :desc => "Pokkarit, järkkärit, skannerit ja kaikki muu digiin liittyvä"
)

testos = User.create!(
  :email => "testos@teroni.fi",
  :password => "secretos",
  :role => "admin"
)

ozil = User.create!(
  :email => "ozil@lali.ga",
  :password => "ozil",
  :role => "basic"
)

jarza = User.create!(
  :email => "jarza@suomi.fi",
  :password => "jarza",
  :role => "basic"
)

jusmu = User.create!(
  :email => "jusmu@laukku.com",
  :password => "jusmu",
  :role => "basic"
)

jmies = User.create!(
  :email => "jmies@kam.pi",
  :password => "jmies",
  :role => "basic"
)

keskikoon_kamerat = Topic.create!(
  :forum_id => filmi.id,
  :title => "Keskikoon kamerat",
  :content => "Minkälaisia keskikoon kameroita löytyy? Hintaluokat ja laadut. 
               Filmit, kasetit ja makasiinit. Minkä hintaisia koneet on. 
               Löytyykö materiaalia vielä hyvin? Ja kaikkea muuta mainitsemisen arvoista.",
  :user_id => ozil.id,
)

keskikoon_kamerat.messages.create!(
  :content => "Hasselilta löytyy ainakin suosittu CW503:nen, johon löyt vielä hyvin osia.",
  :user_id => jmies.id
)

mika_filmi_keskikokoon = Topic.create!(
  :forum_id => filmi.id,
  :title => "Mikä filmi keskikokoon?",
  :content => "Kun tuli nyt hommattua toi mamiya 645 niin pitäis osata sopivaa filmiä hommata.
               Valinta vaan on nyt vaikeeta. Kinarikoossa olen käyttänyt tmaxia ja tri-x:aa.
               Fp5:tta olen joskus kanssa käyttänyt. Tmaxista en oikeen tykännyt mutta siihen 
               on varmaan syy kehittäminen rodinalilla, trix:lla se toimii ihan jees.
               Ja rodinalia varmaan käytän jatkossakin kun sitä on tuolla kaapissa vielä reilu pari pulloa.",
  :user_id => jarza.id
)

mika_filmi_keskikokoon.messages.create!(
  :content => "Olen kuvaillut tässä hieman värille, mutta ajatuksena on tilailla ulkomailta nettikaupasta helvempaa Trixiä ja Acufine kehitettä. Ainakin ne kuvat joita olen nähnyt tuolla yhdistelmällä ovat olleet mun mieleen. Rakeesta tulee hienon oloinen. Rodinal pitäis kokeilla myös Trixin kanssa.",
  :user_id => jusmu.id
)

mika_filmi_keskikokoon.messages.create!(
  :content => "Miksi et jatka suoraan sillä tri-x:llä jos se on hyväksi todettu?",
  :user_id => testos.id
)

valokuvien_organisointi = Topic.create!(
  :forum_id => digi.id,
  :title => "Valokuvien organisointi",
  :content => "Olen kuvannut vasta puolisen vuotta, ja jo nyt on tietokone täynnä valokuvia. 
Valokuvia siellä täällä, eikä \"niitä kuvia\" ikinä löydy - siis niitä joita etsii! 
(nytkin oon kadottanu philips-tuotetestikuvat, v*tuttaa) Käytössä Windows 7 ja 
3 kappaletta kiintolevyjä (500 gt, 500 gt ja 160 gigaa) Miten digikuvat kannattaa teidän mielestä 
järjestellä koneelle? Millä kriteerein? Miten te järjestätte?",
  :user_id => jusmu.id
)

