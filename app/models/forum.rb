class Forum < ActiveRecord::Base
  attr_accessible :desc, :name

  has_many :topics
  has_many :messages, :through => :topics

  validates_presence_of :name, :desc
  validates_uniqueness_of :name

  def recent_message
    messages.last
  end
end
