class Topic < ActiveRecord::Base
  attr_accessible :content, :title, :user_id, :forum_id

  belongs_to :forum
  belongs_to :user
  has_many :messages

  validates_presence_of :title, :content, :forum_id, :user_id
end
