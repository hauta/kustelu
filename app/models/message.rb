class Message < ActiveRecord::Base
  attr_accessible :content, :title, :user_id, :topic_id

  belongs_to :forum
  belongs_to :topic
  belongs_to :user
end
