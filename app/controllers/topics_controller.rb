class TopicsController < ApplicationController
  def show
    @topic = Topic.find(params[:id])
  end

  def new
    @forum = Forum.find(params[:forum_id])
    @topic = @forum.topics.build
  end

  def create
    @topic = Topic.new(params[:topic])
    if @topic.save
      redirect_to @topic
    else
      render :new
    end
  end
end
