class ForumsController < ApplicationController
  before_filter :authorize!, :only => [:new]

  def index
    @forums = Forum.all
  end

  def show
    @forum = Forum.find(params[:id])
  end

  def new
    @forum = Forum.new
  end

  def create
    @forum = Forum.new(params[:forum])
    if @forum.save
      redirect_to @forum
    else
      render :new
    end
  end

  private
  def authorize!
    if !logged_in?
      redirect_to login_url
    elsif !admin?
      raise ActionController::RoutingError.new('Not Found')
    end
  end
end
