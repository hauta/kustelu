class ApplicationController < ActionController::Base
  protect_from_forgery
  helper_method :current_user, :logged_in?, :admin?

  rescue_from ActionController::RoutingError do
    render :file => "public/404.html"
  end

  private
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def logged_in?
    current_user != nil
  end

  def admin?
    current_user.role?(:admin) if logged_in?
  end
end
