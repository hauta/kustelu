class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.authenticate(params[:email], params[:password])
    if user
      session[:user_id] = user.id
      redirect_to root_url
    else
      flash.now.alert = "Email and/or password mismatch"
      render :new
    end
  end

  def destroy
    @current_user = session[:user_id] = nil
    redirect_to root_url
  end
end

