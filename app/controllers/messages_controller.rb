class MessagesController < ApplicationController
  def create
    @topic = Topic.find(params[:topic_id])
    message = @topic.messages.build(params[:message])
    if message.save
      redirect_to @topic
    end
  end
end
